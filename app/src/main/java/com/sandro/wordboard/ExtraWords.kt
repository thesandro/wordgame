package com.sandro.wordboard

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_extra_words.*
import java.lang.StringBuilder

class ExtraWords : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extra_words)
        correctExtrasTextView.text = convertToGeorgian(intent.extras!!.getString("EXTRAS")!!)

    }

    fun convertToGeorgian(str:String):String
    {
        val strBuild = StringBuilder(str)
        for(i:Int in 0 until str.length)
        {
            if(strBuild[i] in 'A'..'z')
            {
                when(strBuild[i])
                {
                    'a'->strBuild[i]='ა'
                    'b'->strBuild[i]='ბ'
                    'g'->strBuild[i]='გ'
                    'd'->strBuild[i]='დ'
                    'e'->strBuild[i]='ე'
                    'v'->strBuild[i]='ვ'
                    'z'->strBuild[i]='ზ'
                    'T'->strBuild[i]='თ'
                    'i'->strBuild[i]='ი'
                    'k'->strBuild[i]='კ'
                    'l'->strBuild[i]='ლ'
                    'm'->strBuild[i]='მ'
                    'n'->strBuild[i]='ნ'
                    'o'->strBuild[i]='ო'
                    'p'->strBuild[i]='პ'
                    'J'->strBuild[i]='ჟ'
                    'r'->strBuild[i]='რ'
                    's'->strBuild[i]='ს'
                    't'->strBuild[i]='ტ'
                    'u'->strBuild[i]='უ'
                    'f'->strBuild[i]='ფ'
                    'q'->strBuild[i]='ქ'
                    'R'->strBuild[i]='ღ'
                    'y'->strBuild[i]='ყ'
                    'S'->strBuild[i]='შ'
                    'C'->strBuild[i]='ჩ'
                    'c'->strBuild[i]='ც'
                    'Z'->strBuild[i]='ძ'
                    'w'->strBuild[i]='წ'
                    'W'->strBuild[i]='ჭ'
                    'x'->strBuild[i]='ხ'
                    'j'->strBuild[i]='ჯ'
                    'h'->strBuild[i]='ჰ'
                }

            }
        }
        return strBuild.toString()
    }
}
