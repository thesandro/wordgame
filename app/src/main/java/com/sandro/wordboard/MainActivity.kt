package com.sandro.wordboard

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.AlphaAnimation
import kotlinx.android.synthetic.main.activity_main.*
import com.sandro.wordboard.instruction.InstructionActivity



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppThemeGame)
        val buttonClick = AlphaAnimation(1f, 0.8f)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

//        buttonEffect(start)
//        buttonEffect(levels)
        start.setOnClickListener {view->
            view.startAnimation(buttonClick)
            val intent = Intent(this, GameActivity::class.java).apply {
                putExtra("START","startbutton")
            }
            startActivity(intent)
        }
        levels.setOnClickListener {view->
            view.startAnimation(buttonClick)
            val intent = Intent(this, Levels::class.java)
            startActivity(intent)

        }
        instructionsButton.setOnClickListener {
            val intent = Intent(this,InstructionActivity::class.java)
            startActivity(intent)
        }

    }
}
