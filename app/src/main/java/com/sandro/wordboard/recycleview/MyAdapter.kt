package com.sandro.wordboard.recycleview

import android.app.Activity
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sandro.wordboard.R
import com.sandro.wordboard.GameActivity
import kotlinx.android.synthetic.main.reyclce_view_layout.view.*


class MyAdapter(private val myDataset: ArrayList<String>,private val maxLevel:Int): RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    //To hold View (Duh)
    class MyViewHolder(val myView: View) : RecyclerView.ViewHolder(myView)
    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val myView = LayoutInflater.from(parent.context)
            .inflate(R.layout.reyclce_view_layout, parent, false) as View
        Log.i("testcreate","boom")

        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(myView)
    }
    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        // Extract Dataset and set it as textView text
        if(position<=maxLevel) {
            holder.myView.setOnClickListener {
                val intent = Intent(holder.myView.context, GameActivity::class.java).apply {
                    putExtra("LEVEL", position)
                }

                holder.myView.context.startActivity(intent)
                (holder.myView.context as Activity).finish()
            }
            holder.myView.linearLayoutForRecycleView.setBackgroundColor(ContextCompat.getColor(holder.myView.context,R.color.woody))
            holder.myView.imageViewForRecycleView.background = null
        }
        else {
            holder.myView.linearLayoutForRecycleView.setOnClickListener(null)
            holder.myView.linearLayoutForRecycleView.setBackgroundColor(ContextCompat.getColor(holder.myView.context,R.color.notAvailable))
            holder.myView.imageViewForRecycleView.background = (ContextCompat.getDrawable(holder.myView.context,R.drawable.block))
        }
        holder.myView.textViewForRecycleView.text = myDataset[position]

    }
    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}