package com.sandro.wordboard.letterpattern

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.View


@SuppressLint("ViewConstructor")
class Cell(context: Context,
           var index: Int,
           private var regularDotColor: Int,
           private var selectedDotColor: Int,
           private var selectedDotRadiusRatio: Float,
           private var lineStyle: Int,
           private var regularLineColor: Int,
           private var indicatorSizeRatio: Float,
           val letter:Char
) : View(context) {

    private var currentState: State = State.REGULAR
    private var paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var currentDegree: Float = -1f
    private var indicatorPath: Path = Path()


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = 50
        val desiredHeight = 50

        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val width: Int
        val height: Int

        //Measure Width
        if (widthMode == View.MeasureSpec.EXACTLY) {
            width = widthSize
        } else if (widthMode == View.MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize)
        } else {
            width = desiredWidth
        }

        //Measure Height
        if (heightMode == View.MeasureSpec.EXACTLY) {
            height = heightSize
        } else if (heightMode == View.MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize)
        } else {

            height = desiredHeight
        }
        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas?) {
        when(currentState) {
            State.REGULAR -> drawDot(canvas,regularDotColor)
            State.SELECTED -> drawDot(canvas, selectedDotColor)
        }
    }
    private fun drawDot(canvas: Canvas?,
                        dotColor: Int) {

        if(dotColor == selectedDotColor)
        {
            paint.color = dotColor
            paint.style = Paint.Style.FILL
        }
        else if(dotColor == regularDotColor)
        {
            paint.color = dotColor
            paint.style = Paint.Style.FILL
        }

        val bMapOptions = BitmapFactory.Options()

        val letterBitMap:Bitmap = if(letter.isUpperCase()) {
            BitmapFactory.decodeResource(
                resources,
                resources.getIdentifier(letter.toLowerCase() + "_upper" +"_letter", "drawable", context.packageName),bMapOptions)!!
        } else {
            BitmapFactory.decodeResource(
                resources,
                resources.getIdentifier(letter + "_letter", "drawable", context.packageName),bMapOptions
            )
        }
        val scaled = Bitmap.createScaledBitmap(letterBitMap, width, height, true)!!
        bMapOptions.inBitmap = letterBitMap
        letterBitMap.recycle()

        canvas?.drawBitmap(scaled,0f,0f,paint)
        if (lineStyle == LetterPatternView.LINE_STYLE_INDICATOR &&
                (currentState == State.SELECTED)) {
            drawIndicator(canvas)
        }
    }

    private fun drawIndicator(canvas: Canvas?) {
        if (currentDegree != -1f) {
            if (indicatorPath.isEmpty) {
                indicatorPath.fillType = Path.FillType.WINDING
                val radius = getRadius()
                val height = radius * indicatorSizeRatio
                indicatorPath.moveTo((width / 2).toFloat() , radius * (1 - selectedDotRadiusRatio - indicatorSizeRatio) / 2 + paddingTop)
                indicatorPath.lineTo((width /2).toFloat() - height, radius * (1 - selectedDotRadiusRatio - indicatorSizeRatio) / 2 + height + paddingTop)
                indicatorPath.lineTo((width / 2).toFloat() + height, radius * (1 - selectedDotRadiusRatio - indicatorSizeRatio) / 2 + height + paddingTop)
                indicatorPath.close()
            }

            if (currentState == State.SELECTED) {
                paint.color = regularLineColor
            }
            paint.style = Paint.Style.FILL

            canvas?.save()
            canvas?.rotate(currentDegree, (width / 2).toFloat(), (height / 2).toFloat())
            canvas?.drawPath(indicatorPath, paint)
            canvas?.restore()
        }
    }

    fun getRadius() : Int {
        return (Math.min(width, height) - (paddingLeft + paddingRight)) / 2
    }


    fun getCenter() : Point {
        val point = Point()
        point.x = left + (right - left) / 2
        point.y = top + (bottom - top) / 2
        return point
    }

    fun setState(state: State) {
        currentState = state
        invalidate()
    }

    fun setDegree(degree: Float) {
        currentDegree = degree
    }

    fun reset() {
        setState(State.REGULAR)
        currentDegree = -1f
    }

 }