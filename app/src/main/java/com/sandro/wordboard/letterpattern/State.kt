package com.sandro.wordboard.letterpattern


enum class State {
    REGULAR, SELECTED
}