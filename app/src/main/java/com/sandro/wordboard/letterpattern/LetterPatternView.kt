package com.sandro.wordboard.letterpattern

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.sandro.wordboard.R
import com.sandro.wordboard.worddisplay.Box
import java.util.*


class LetterPatternView : RelativeLayout {
    companion object {
        const val DEFAULT_RADIUS_RATIO = 1f
        const val DEFAULT_LINE_WIDTH = 2f // unit: dp
        const val DEFAULT_SPACING = 24f // unit: dp
        const val DEFAULT_INDICATOR_SIZE_RATIO = 0.2f

        const val LINE_STYLE_COMMON = 1
        const val LINE_STYLE_INDICATOR = 2
        const val DEFAULT_BOX_SIZE = 70f // unit:dp
    }

    private var regularDotColor: Int = 0
    var selectedDotColor: Int = 0
    private var selectedDotRadiusRatio: Float = 0f

    private var boxSize:Int = 0
    private var lineStyle: Int = 0

    private var lineWidth: Int = 0
    private var regularLineColor: Int = 0

    private var spacing: Int = 0

    private var hitAreaPaddingRatio: Float = 0f
    private var indicatorSizeRatio: Float = 0f

    private var cells: ArrayList<Cell> = ArrayList()
    private var selectedCells: ArrayList<Cell> = ArrayList()

    private var linePaint: Paint = Paint()
    private var linePath: Path = Path()

    private var lastX: Float = 0f
    private var lastY: Float = 0f
    private var regularDotColorForTemp: Int = 0
    private var selectedDotColorForTemp: Int =0


    private var onPatternListener: OnPatternListener? = null

    lateinit var tempWordLayout:RelativeLayout
        private set
    // rom gavigo garedan romeli yuti gamoiyeneba
    var currentBoxContainer:LinearLayout? = null
    //testing
    var anagram:String = ""

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        val ta = context.obtainStyledAttributes(attributeSet, R.styleable.LetterPatternView)
        regularDotColor = ta.getColor(R.styleable.LetterPatternView_lpv_regularDotColor, ContextCompat.getColor(context, R.color.regularColor))
        selectedDotColor = ta.getColor(R.styleable.LetterPatternView_lpv_selectedDotColor, ContextCompat.getColor(context, R.color.selectedColor))
        selectedDotRadiusRatio = ta.getFloat(R.styleable.LetterPatternView_lpv_selectedDotRadiusRatio, DEFAULT_RADIUS_RATIO)

        lineStyle = ta.getInt(R.styleable.LetterPatternView_lpv_lineStyle, 1)
        lineWidth = ta.getDimensionPixelSize(R.styleable.LetterPatternView_lpv_lineWidth,
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_LINE_WIDTH, context.resources.displayMetrics).toInt())
        regularLineColor = ta.getColor(R.styleable.LetterPatternView_lpv_regularLineColor,ContextCompat.getColor(context,R.color.regularLineColor))

        spacing = ta.getDimensionPixelSize(
            R.styleable.LetterPatternView_lpv_spacing,
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_SPACING, context.resources.displayMetrics).toInt())
        indicatorSizeRatio = ta.getFloat(R.styleable.LetterPatternView_lpv_indicatorSizeRatio, DEFAULT_INDICATOR_SIZE_RATIO)
        boxSize = ta.getDimensionPixelSize(R.styleable.LetterPatternView_lpv_box_size,
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_BOX_SIZE, context.resources.displayMetrics).toInt())

        regularDotColorForTemp = ta.getColor(R.styleable.WordDisplayView_wdv_regularDotColor, ContextCompat.getColor(context, R.color.regularBoxColor))
        selectedDotColorForTemp = ta.getColor(R.styleable.WordDisplayView_wdv_selectedDotColor, ContextCompat.getColor(context, R.color.filledBoxColor))

        tempWordLayout = RelativeLayout(context)
        tempWordLayout.clipChildren= false
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        tempWordLayout.gravity=Gravity.CENTER
        tempWordLayout.layoutParams=params

        //bringing to front
        addView(tempWordLayout)
        ta.recycle()
        initPathPaint()
    }
    private fun addTempBox(hitCell: Cell, tempBoxContainer:LinearLayout)
    {
        val cell = Box(
            context, regularDotColorForTemp, selectedDotColorForTemp, hitCell.letter)
        cell.setState(com.sandro.wordboard.worddisplay.State.FILLED)
        val cellparam = LinearLayout.LayoutParams(boxSize*3/4, boxSize*3/4)
        cellparam.setMargins(boxSize / 16, boxSize / 16, boxSize / 16, boxSize/16)
        cell.layoutParams = cellparam
        tempBoxContainer.addView(cell)
    }
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.action) {
            MotionEvent.ACTION_DOWN ->{
                val hitCell = getHitCell(event.x.toInt(), event.y.toInt())
                if (hitCell != null) {
                    onPatternListener?.onStarted()
                    notifyCellSelected(hitCell)
                    //sityvis awyoba ro iwyeba iqmneba axali LinearLayout asoebis mosatavseblad
                    val tempWordContainer = LinearLayout(context)
                    val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT)
                    params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
                    tempWordContainer.layoutParams=params
                    tempWordContainer.gravity= Gravity.CENTER
                    tempWordLayout.addView(tempWordContainer)
                    currentBoxContainer = tempWordContainer
                    tempWordContainer.setBackgroundColor(selectedDotColor)
                    addTempBox(hitCell,tempWordContainer)
                }
            }

            MotionEvent.ACTION_MOVE -> {
                if(currentBoxContainer !=null)
                 handleActionMove(event,currentBoxContainer!!)
                else
                {
                    val tempWordContainer = LinearLayout(context)
                    val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,boxSize)
                    params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
                    tempWordContainer.layoutParams=params
                    tempWordContainer.gravity= Gravity.CENTER
                    tempWordLayout.addView(tempWordContainer)
                    currentBoxContainer = tempWordContainer
                    tempWordContainer.setBackgroundColor(selectedDotColor)
                    handleActionMove(event,currentBoxContainer!!)
                }

        }


            MotionEvent.ACTION_UP -> {
                onFinish()
            }

            MotionEvent.ACTION_CANCEL -> reset()

            else -> return false
        }
        return true
    }

    private fun handleActionMove(event: MotionEvent,currentBoxContainer: LinearLayout) {
        val hitCell = getHitCell(event.x.toInt(), event.y.toInt())
        if (hitCell != null) {
            if (!selectedCells.contains(hitCell)) {
                notifyCellSelected(hitCell)
                addTempBox(hitCell,currentBoxContainer)
            }
        }

        lastX = event.x
        lastY = event.y

        invalidate()
    }

    private fun notifyCellSelected(cell: Cell) {
        selectedCells.add(cell)
        onPatternListener?.onProgress(generateSelectedIds())
        cell.setState(State.SELECTED)
        val center = cell.getCenter()
        if (selectedCells.size == 1) {
            if (lineStyle == LINE_STYLE_COMMON) {
                linePath.moveTo(center.x.toFloat(), center.y.toFloat())
            }
        } else {
            if (lineStyle == LINE_STYLE_COMMON) {
                linePath.lineTo(center.x.toFloat(), center.y.toFloat())
            } else if (lineStyle == LINE_STYLE_INDICATOR) {
                val previousCell = selectedCells[selectedCells.size - 2]
                val previousCellCenter = previousCell.getCenter()
                val diffX = center.x - previousCellCenter.x
                val diffY = center.y - previousCellCenter.y
                val radius = cell.getRadius()
                val length = Math.sqrt((diffX * diffX + diffY * diffY).toDouble())

                linePath.moveTo((previousCellCenter.x + radius * diffX / length).toFloat(), (previousCellCenter.y + radius * diffY / length).toFloat())
                linePath.lineTo((center.x - radius * diffX / length).toFloat(), (center.y - radius * diffY / length).toFloat())

                val degree = Math.toDegrees(Math.atan2(diffY.toDouble(), diffX.toDouble())) + 90
                previousCell.setDegree(degree.toFloat())
                previousCell.invalidate()
            }
        }
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        canvas?.drawPath(linePath, linePaint)
        if (selectedCells.size > 0 && lastX > 0) { //  && lastX > 0 && lastY > 0
            if (lineStyle == LINE_STYLE_COMMON) {
                val center = selectedCells[selectedCells.size - 1].getCenter()
                canvas?.drawLine(center.x.toFloat(), center.y.toFloat(), lastX, lastY, linePaint)
            } else if (lineStyle == LINE_STYLE_INDICATOR) {
                val lastCell = selectedCells[selectedCells.size - 1]
                val lastCellCenter = lastCell.getCenter()
                val radius = lastCell.getRadius()

                if (!(lastX >= lastCellCenter.x - radius &&
                        lastX <= lastCellCenter.x + radius &&
                        lastY >= lastCellCenter.y - radius &&
                        lastY <= lastCellCenter.y + radius)) {
                    val diffX = lastX - lastCellCenter.x
                    val diffY = lastY - lastCellCenter.y
                    val length = Math.sqrt((diffX * diffX + diffY * diffY).toDouble())
                    canvas?.drawLine((lastCellCenter.x + radius * diffX / length).toFloat(),
                            (lastCellCenter.y + radius * diffY / length).toFloat(),
                            lastX, lastY, linePaint)
                }
            }
        }

    }

    private fun setupCells() {
        when(anagram.length) {
            6->hexagonArrangement()
            5->pentagonArrangement()
            4->squareArrangement()
            3->triangleArrangement()
        }
    }

    private fun triangleArrangement()
    {
        for(i in 0..(3-1)) {
            val cell = Cell(context, i, regularDotColor, selectedDotColor, selectedDotRadiusRatio,
                lineStyle, regularLineColor, indicatorSizeRatio, anagram[i]
            )
            cell.id = 1111+i

            addView(cell)
            cells.add(cell)

        }
        val lsize = boxSize

        var params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        params.topMargin= boxSize
//        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
//        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        cells[0].layoutParams=params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_START,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginStart = boxSize+boxSize/4
        params.topMargin= boxSize
        cells[1].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_END,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginEnd = boxSize+boxSize/4
        params.topMargin=boxSize
        cells[2].layoutParams = params

    }

    private fun squareArrangement()
    {
        for(i in 0..(4-1)) {
            val cell = Cell(context, i, regularDotColor, selectedDotColor, selectedDotRadiusRatio,
                lineStyle, regularLineColor, indicatorSizeRatio, anagram[i]
            )
            cell.id = 1111+i

            addView(cell)
            cells.add(cell)

        }
        val lsize = boxSize

        var params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        params.topMargin= boxSize
//        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
//        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        cells[0].layoutParams=params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_START,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginStart = boxSize+boxSize/4
        params.topMargin= boxSize/4
        cells[1].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_END,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginEnd = boxSize+boxSize/4
        params.topMargin=boxSize/4
        cells[2].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.BELOW,cells[1].id)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        params.topMargin=boxSize/4
        cells[3].layoutParams = params

    }
    private fun pentagonArrangement()
    {
        val lsize = boxSize

        for(i in 0..(5-1)) {
                val cell = Cell(context, i, regularDotColor, selectedDotColor, selectedDotRadiusRatio,
                    lineStyle, regularLineColor, indicatorSizeRatio, anagram[i]
                )
                cell.id = 1111+i
                addView(cell)
                cells.add(cell)

        }
        var params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        params.topMargin= boxSize
        cells[0].layoutParams=params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_START,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)

        params.marginStart = boxSize+boxSize/4
        cells[1].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_END,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginEnd = boxSize+boxSize/4
        cells[2].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_START,cells[1].id)
        params.addRule(RelativeLayout.BELOW,cells[1].id)
        params.marginStart = -boxSize/2
        params.topMargin = boxSize/4
        cells[3].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_END,cells[2].id)
        params.addRule(RelativeLayout.BELOW,cells[2].id)
        params.marginEnd = -boxSize/2
        params.topMargin = boxSize/4
        cells[4].layoutParams = params
 }
    private fun hexagonArrangement()
    {
        val lsize = boxSize
        for(i in 0..(6-1)) {
            val cell = Cell(context, i, regularDotColor, selectedDotColor, selectedDotRadiusRatio,
                lineStyle, regularLineColor, indicatorSizeRatio, anagram[i]
            )
            cell.id = 1111+i
            addView(cell)
            cells.add(cell)

        }
        var params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        params.topMargin= boxSize
        cells[0].layoutParams=params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_START,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginStart = boxSize+boxSize/2
        params.topMargin = boxSize/4
        cells[1].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_END,cells[0].id)
        params.addRule(RelativeLayout.BELOW,cells[0].id)
        params.marginEnd = boxSize+boxSize/2
        params.topMargin = boxSize/4
        cells[2].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_START,cells[1].id)
        params.addRule(RelativeLayout.BELOW,cells[1].id)
        params.topMargin = boxSize/4
        cells[3].layoutParams = params

        params = RelativeLayout.LayoutParams(lsize,lsize)
        params.addRule(RelativeLayout.ALIGN_END,cells[2].id)
        params.addRule(RelativeLayout.BELOW,cells[1].id)
        params.topMargin = boxSize/4
        cells[4].layoutParams = params
        params = RelativeLayout.LayoutParams(lsize, lsize)
        params.addRule(RelativeLayout.BELOW,cells[3].id)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE)
        params.topMargin = boxSize/4
        cells[5].layoutParams = params

    }


    private fun initPathPaint() {
        linePaint.isAntiAlias = true
        linePaint.isDither = true
        linePaint.style = Paint.Style.STROKE
        linePaint.strokeJoin = Paint.Join.ROUND
        linePaint.strokeCap = Paint.Cap.ROUND

        linePaint.strokeWidth = lineWidth.toFloat()
        linePaint.color = regularLineColor
    }

    private fun reset() {
        for(cell in selectedCells) {
            cell.reset()
        }
        selectedCells.clear()
        linePaint.color = regularLineColor
        linePath.reset()

        lastX = 0f
        lastY = 0f

        invalidate()
        // ishleba
        currentBoxContainer=null
    }


    private fun getHitCell(x: Int, y: Int) : Cell? {
        for(cell in cells) {
            if (isSelected(cell, x, y)) {
                return cell
            }
        }
        return null
    }

    private fun isSelected(view: View, x: Int, y: Int) : Boolean {
        val innerPadding = view.width * hitAreaPaddingRatio
        return x >= view.left + innerPadding &&
                x <= view.right - innerPadding &&
                y >= view.top + innerPadding &&
                y <= view.bottom - innerPadding
    }

    private fun onFinish() {
        lastX = 0f
        lastY = 0f

        val isCorrect = onPatternListener?.onComplete(generateSelectedIds())
        if (isCorrect != null && isCorrect) {
            reset()
        }
    }

    private fun generateSelectedIds() : ArrayList<Int> {
        val result = ArrayList<Int>()
        for(cell in selectedCells) {
            result.add(cell.index)
        }
        return result
    }
    fun addLetters(str:String)
    {
        for(i in 0 until cells.size)
        removeView(cells[i])
        cells.clear()
        anagram = str
        setupCells()
    }

    fun setOnPatternListener(listener: OnPatternListener) {
        onPatternListener = listener
    }


    interface OnPatternListener {
        fun onStarted(){}
        fun onProgress(ids: ArrayList<Int>){}
        fun onComplete(ids: ArrayList<Int>) : Boolean
    }

}