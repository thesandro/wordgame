package com.sandro.wordboard.worddisplay

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.sandro.wordboard.R
import kotlinx.android.synthetic.main.activity_game.view.*
import java.util.*

class WordDisplayView : LinearLayout {
    companion object {
        const val DEFAULT_RADIUS_RATIO = 1f
        const val DEFAULT_SPACING = 24f // unit: dp
        const val DEFAULT_BOX_SIZE = 30f
    }

    private var regularDotColor: Int = 0
    private var regularDotRadiusRatio: Float = 0f

    private var selectedDotColor: Int = 0
    private var selectedDotRadiusRatio: Float = 0f
    private var spacing: Int = 0
    private var boxes: ArrayList<Box> = ArrayList()
    private var boxSize:Int = 0

    private var anagrams:Array<String> = arrayOf("")

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        val ta = context.obtainStyledAttributes(attributeSet, R.styleable.WordDisplayView)
        regularDotColor = ta.getColor(R.styleable.WordDisplayView_wdv_regularDotColor, ContextCompat.getColor(context, R.color.regularBoxColor))
        regularDotRadiusRatio = ta.getFloat(R.styleable.WordDisplayView_wdv_regularDotRadiusRatio, DEFAULT_RADIUS_RATIO)

        selectedDotColor = ta.getColor(R.styleable.WordDisplayView_wdv_selectedDotColor, ContextCompat.getColor(context, R.color.filledBoxColor))
        selectedDotRadiusRatio = ta.getFloat(R.styleable.WordDisplayView_wdv_selectedDotRadiusRatio, DEFAULT_RADIUS_RATIO)

        spacing = ta.getDimensionPixelSize(
            R.styleable.WordDisplayView_wdv_spacing,
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_SPACING, context.resources.displayMetrics).toInt())
        boxSize = ta.getDimensionPixelSize(R.styleable.WordDisplayView_wdv_box_size,
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_BOX_SIZE, context.resources.displayMetrics).toInt())
        ta.recycle()
        orientation=LinearLayout.VERTICAL
        layoutParams= LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

    }
    private fun setupCells() {
        for(i in 0..(anagrams.size-1)) {
            val line = LinearLayout(context)
            val layoutparam = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutparam.height= 0
            layoutparam.weight=1f
            line.layoutParams= layoutparam
            line.orientation=LinearLayout.HORIZONTAL
            line.gravity= Gravity.CENTER
//            line.setOnClickListener { view->
//                val clickedLine = view as LinearLayout
//                val preferences = context.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE)
//                if((context.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt("coins", 0)-5) >0
//                    && (clickedLine.getChildAt(0) as Box).currentState != State.FILLED ) {
//                    while (true) {
//                        val random = (Math.random() * clickedLine.childCount).toInt()
//                        if ((clickedLine.getChildAt(random) as Box).currentState != State.HINT) {
//                            (clickedLine.getChildAt(random) as Box).setState(State.HINT)
//                            preferences.edit().putInt(
//                                "coins",
//                                context.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt(
//                                    "coins",
//                                    0
//                                ) - 5
//                            ).apply()
//                            val coinText = this.rootView.findViewById<TextView>(R.id.coinsTextView)
//                            coinText.text = context.getString(
//                                R.string.coins, context.getSharedPreferences(
//                                    "PlayerInfo",
//                                    Context.MODE_PRIVATE
//                                ).getInt("coins", 0).toString())
//                            break
//                        }
//                    }
//                }
//            }

            addView(line)
            for(j in 0..(anagrams[i].length-1)) {
                val cell = Box(
                    context, regularDotColor, selectedDotColor, anagrams[i][j]
                )
                val cellparam = LinearLayout.LayoutParams(boxSize, boxSize)
                cellparam.setMargins(boxSize/8,0,boxSize/8,0)
                cell.layoutParams = cellparam
                line.addView(cell)
                boxes.add(cell)

            }
        }
    }

    fun addWords(wordsArray:Array<String>)
    {
        removeAllViews()
        anagrams = wordsArray
        setupCells()
    }
    fun wordChangeState(childIndex:Int)
    {
        val childLinearLayout = getChildAt(childIndex) as ViewGroup

        for(i in 0..(childLinearLayout.childCount-1))
        {
            val letter: Box = childLinearLayout.getChildAt(i) as Box
            letter.setState(State.FILLED)
        }
    }
}