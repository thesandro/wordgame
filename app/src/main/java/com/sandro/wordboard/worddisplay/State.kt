package com.sandro.wordboard.worddisplay


enum class State {
    REGULAR, FILLED,HINT
}