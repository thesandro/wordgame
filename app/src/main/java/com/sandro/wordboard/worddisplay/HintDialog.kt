package com.sandro.wordboard.worddisplay

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sandro.wordboard.R
import kotlinx.android.synthetic.main.activity_hint_dialog.*

class HintDialog : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hint_dialog)
        button1.setOnClickListener{
            intent.putExtra("Answer",false)
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
        button2.setOnClickListener{
            intent.putExtra("Answer",true)
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
        exitButton.setOnClickListener{
            finish()
        }

    }
}
