package com.sandro.wordboard.worddisplay

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import com.sandro.wordboard.R


@SuppressLint("ViewConstructor")
 class Box(
     context: Context,
     private var regularDotColor: Int,
     private var selectedDotColor: Int,
     private val letter: Char
) : View(context) {
    var currentState: State = State.REGULAR
        private set
    private var paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

     override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = 50
        val desiredHeight = 50

        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val width: Int
        val height: Int

        //Measure Width
         width = if (widthMode == View.MeasureSpec.EXACTLY) {
             widthSize
         } else if (widthMode == View.MeasureSpec.AT_MOST) {
             Math.min(desiredWidth, widthSize)
         } else {
             desiredWidth
         }

        //Measure Height
         height = if (heightMode == View.MeasureSpec.EXACTLY) {
             heightSize
         } else if (heightMode == View.MeasureSpec.AT_MOST) {
             Math.min(desiredHeight, heightSize)
         } else {

             desiredHeight
         }
        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas?) {
        when (currentState) {
            State.REGULAR -> drawDot(canvas, regularDotColor)
            State.FILLED -> drawDot(canvas, selectedDotColor,letter,false)
            State.HINT -> drawDot(canvas,selectedDotColor,letter,true)
        }
    }

    private fun drawDot(
        canvas: Canvas?,
        dotColor: Int) {

        paint.color = dotColor
        paint.style = Paint.Style.FILL
        canvas?.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)

    }
    private fun drawDot(
        canvas: Canvas?,
        dotColor: Int,letter: Char,isHint:Boolean) {
        //loading for later reuse
        val bMapOptionsForLetter = BitmapFactory.Options()
        val letterBitMap:Bitmap = if(letter.isUpperCase()) {
            BitmapFactory.decodeResource(
                resources,
                resources.getIdentifier(letter.toLowerCase() + "_upper" +"_letter", "drawable", context.packageName),bMapOptionsForLetter)!!
        } else {
            BitmapFactory.decodeResource(
                resources,
                resources.getIdentifier(letter + "_letter", "drawable", context.packageName),bMapOptionsForLetter
            )!!
        }

        paint.color = dotColor
        paint.style = Paint.Style.FILL
        bMapOptionsForLetter.inBitmap= letterBitMap
        val scaled: Bitmap = Bitmap.createScaledBitmap(letterBitMap, width, height, true)!!
        letterBitMap.recycle()
        canvas?.drawBitmap(scaled, 0f, 0f, paint)
        if(isHint)
        {
            paint.color = ContextCompat.getColor(context, R.color.hintColor)
            canvas?.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        }
    }


     fun setState(state: State) {
        currentState = state
        invalidate()
    }

 }