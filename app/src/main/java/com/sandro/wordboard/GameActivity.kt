package com.sandro.wordboard

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.sandro.wordboard.letterpattern.LetterPatternView
import com.sandro.wordboard.worddisplay.Box
import com.sandro.wordboard.worddisplay.HintDialog
import com.sandro.wordboard.worddisplay.State
import kotlinx.android.synthetic.main.activity_game.*
import java.util.*
import com.facebook.ads.*;
import com.facebook.ads.Ad
import com.facebook.ads.AdError




class GameActivity : AppCompatActivity(){
    val buttonClick = AlphaAnimation(1f, 0.8f)
    private var rewardedVideoAd: RewardedVideoAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        reLoadAd()
        val res = resources
        val challenge: ArrayList<Array<String>> = ArrayList()
        val extras: ArrayList<Array<String>> = ArrayList()
        val ta = res.obtainTypedArray(R.array.levels)
        val tae = res.obtainTypedArray(R.array.extras)
        for (i in 0 until ta.length()) {
            challenge.add(res.getStringArray(ta.getResourceId(i, 0)))
        }
        for (i in 0 until tae.length()) {
            extras.add(res.getStringArray(tae.getResourceId(i, 0)))
        }

        ta.recycle()
        tae.recycle()

        startGame(challenge, extras)
    }
    private fun reLoadAd()
    {
        rewardedVideoAd = RewardedVideoAd(this, "YOUR_PLACEMENT_ID")
        rewardedVideoAd!!.setAdListener(object : RewardedVideoAdListener {
            override fun onError(ad: Ad, error: AdError) {
                // Rewarded video ad failed to load
                Log.e("testvideoad", "Rewarded video ad failed to load: " + error.errorMessage)
            }

            override fun onAdLoaded(ad: Ad) {
                // Rewarded video ad is loaded and ready to be displayed
                hintButton.visibility= View.VISIBLE
                Log.d("testvideoad", "Rewarded video ad is loaded and ready to be displayed!")
            }

            override fun onAdClicked(ad: Ad) {
                // Rewarded video ad clicked
                Log.d("testvideoad", "Rewarded video ad clicked!")
            }

            override fun onLoggingImpression(ad: Ad) {
                // Rewarded Video ad impression - the event will fire when the
                // video starts playing
                Log.d("testvideoad", "Rewarded video ad impression logged!")
            }

            override fun onRewardedVideoCompleted() {

                // Rewarded Video View Complete - the video has been played to the end.
                // You can use this event to initialize your reward
                Log.d("testvideoad", "Rewarded video completed!")
                hintButton.visibility= View.INVISIBLE
                reLoadAd()
                // Call method to give reward
                giveReward(10)
            }

            override fun onRewardedVideoClosed() {
                // The Rewarded Video ad was closed - this can occur during the video
                // by closing the app, or closing the end card.

                Log.d("testvideoad", "Rewarded video ad closed!")
            }
        })
        rewardedVideoAd?.loadAd()
    }
    @SuppressLint("SetTextI18n")
    private fun startGame(challenge: ArrayList<Array<String>>, extras: ArrayList<Array<String>>) {
        val ta =resources.obtainTypedArray(R.array.levels)
        val size = ta.length()
        ta.recycle()

        val correctExtraWords = StringBuilder()
        var preferences = applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE)
        coinsTextView.text = getString(
            R.string.coins,
            applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt(
                "coins",
                0
            ).toString()
        )
        var currentChallengeIndex = preferences.getInt("currentlevel", 0)


        var correctChallenge = BooleanArray(challenge[currentChallengeIndex].size)
        var correctExtras = BooleanArray(extras[currentChallengeIndex].size)


        if (intent.extras != null) {

            if (intent.extras?.get("LEVEL") != null) {
                Log.i("bubababa", "I greet you: " + intent.extras!!.getInt("LEVEL"))
                currentChallengeIndex = intent.extras!!.getInt("LEVEL")
                correctChallenge = BooleanArray(challenge[currentChallengeIndex].size)
                correctExtras = BooleanArray(extras[currentChallengeIndex].size)
                preferences.edit().putInt("currentlevel", currentChallengeIndex).apply()
                storeArray(correctChallenge, "currentProgress", applicationContext)
                storeArray(correctExtras, "currentExtraProgress", applicationContext)
            }
        }
        // level display
        currentLevelDisplay.text = "${(currentChallengeIndex + 1)}/${(size + 1)}"
        letterPatternView.addLetters(shuffle(challenge[currentChallengeIndex][0]))
        wordDisplayView.addWords(challenge[currentChallengeIndex])
        if (intent.extras != null) {
            if (intent.extras?.getString("START") == "startbutton") {
                for (i in 0 until correctChallenge.size)
                    correctChallenge[i] = preferences.getBoolean("currentProgress_$i", false)
                for (i in 0 until correctExtras.size)
                    correctExtras[i] = preferences.getBoolean("currentExtraProgress_$i", false)

                for (i in 0 until correctChallenge.size) {
                    if (correctChallenge[i]) {
                        wordDisplayView.wordChangeState(i)
                    }
                }
                for (i in 0 until correctExtras.size) {
                    if (correctExtras[i]) {
                        correctExtraWords.append(extras[currentChallengeIndex][i] + " ")
                    }
                }
            }
        }
        attachOnClickListeners()


        //Todo: background image (dizainis optimizacia)
        //Todo: რეკლამა
        //Todo: boqlomi gauxsenl levels

        extraWordsButton.setOnClickListener { view ->
            view.startAnimation(buttonClick)
            val intent = Intent(this, ExtraWords::class.java).apply {
                putExtra("EXTRAS", correctExtraWords.toString())
            }
            startActivity(intent)
        }

        hintButton.setOnClickListener { view ->
            view.startAnimation(buttonClick)
            if (rewardedVideoAd!!.isAdLoaded) {
                rewardedVideoAd!!.show()
            }
        }
        shuffleButton.setOnClickListener { view ->
            view.startAnimation(buttonClick)
            if (letterPatternView.currentBoxContainer == null) {
                //            val animation = TranslateAnimation(
//                0f, relativeLayout.width/ 2-shuffleButton.width/2 - shuffleButton.x,
//                0f, relativeLayout.height / 2 - shuffleButton.height/2- shuffleButton.y
//            )
//            animation.repeatMode = 0
//            animation.duration = 3000
//            animation.fillAfter = true
//            shuffleButton.startAnimation(animation)

                shuffleToCenterForAnimationListener(
                    letterPatternView.getChildAt(letterPatternView.childCount - 1),
                    challenge[currentChallengeIndex][0]
                )
                for (i: Int in 1 until letterPatternView.childCount - 1) {
                    shuffleToCenter(letterPatternView.getChildAt(i), 600)
                }
//            letterPatternView.addLetters(shuffle(challenge[currentChallengeIndex][0]))
            }

        }
        letterPatternView.setOnPatternListener(object : LetterPatternView.OnPatternListener {
            //            override fun onStarted() {
//                super.onStarted()
//            }
//            override fun onProgress(ids: ArrayList<Int>) {
//                super.onProgress(ids)
//            }
            override fun onComplete(ids: ArrayList<Int>): Boolean {
                var pattern = ""
                val shuffledLetters = letterPatternView.anagram

                for (i in 0 until ids.size) {
                    pattern += shuffledLetters[ids[i]]
                }
                // if correct
                var correctFound = false
                for (i in 0 until (challenge[currentChallengeIndex].size)) {
                    if (challenge[currentChallengeIndex][i] == pattern) {
                        //aqedan amovshale
                        // wordDisplayView.wordChangeState(i)

                        // tempBox
                        correctFound = true
                        val tempBox = letterPatternView.currentBoxContainer
                        if (!correctChallenge[i]) {
                            for (k: Int in 0 until tempBox!!.childCount) {
//                            val tempBoxLayout= letterPatternView.getChildAt(0) as LinearLayout

                                val wordDisplayContainer = wordDisplayView.getChildAt(i) as LinearLayout
                                animateToCorrectAnswer(
                                    tempBox.getChildAt(k),
                                    wordDisplayContainer.getChildAt(k),
                                    1000,
                                    i,
                                    tempBox
                                )
                            }
                        } else
                            correctFound = false
                        correctChallenge[i] = true

                    }
                }
                for (i in 0 until (extras[currentChallengeIndex].size)) {
                    if (extras[currentChallengeIndex][i] == pattern) {
                        correctFound = true
                        if (!correctExtras[i]) {
                            val tempBox = letterPatternView.currentBoxContainer

                            for (k: Int in 0 until tempBox!!.childCount) {
                                animateToExtras(
                                    tempBox.getChildAt(k),
                                    extraWordsButton,
                                    1000, tempBox
                                )
                            }
                            correctExtraWords.append("$pattern ")
                            correctExtras[i] = true
                        } else
                            correctFound = false
                    }

                }
                if (!correctFound) {

                    letterPatternView.currentBoxContainer?.removeAllViews()
                    val parent = letterPatternView.getChildAt(0) as RelativeLayout
                    parent.removeView(letterPatternView.currentBoxContainer)
                    letterPatternView.currentBoxContainer = null

                }
                storeArray(correctChallenge, "currentProgress", applicationContext)
                storeArray(correctExtras, "currentExtraProgress", applicationContext)
                if (isBooleanArrayTrue(correctChallenge)) {
                    val lastBox =
                        (letterPatternView.getChildAt(0) as RelativeLayout).getChildAt((letterPatternView.getChildAt(0) as RelativeLayout).childCount - 1)
                    lastBox.postDelayed(
                        {

                            preferences = applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE)
                            currentChallengeIndex += 1

                            preferences.edit().putInt("currentlevel", currentChallengeIndex).apply()

                            if (preferences.getInt("maxlevel", 0) < currentChallengeIndex) {
                                preferences.edit().putInt(
                                    "coins",
                                    applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt(
                                        "coins",
                                        0
                                    ) + 2
                                ).apply()
                                preferences.edit().putInt("maxlevel", currentChallengeIndex).apply()
                                coinsTextView.text = getString(
                                    R.string.coins, applicationContext.getSharedPreferences(
                                        "PlayerInfo",
                                        Context.MODE_PRIVATE
                                    ).getInt("coins", 0).toString()
                                )
                            }
                            for (i in 0 until correctChallenge.size)
                                preferences.edit().remove("currentProgress_$i").apply()
                            for (i in 0 until correctExtras.size)
                                preferences.edit().remove("currentExtraProgress_$i").apply()
                            correctChallenge = BooleanArray(challenge[currentChallengeIndex].size)
                            correctExtras = BooleanArray(extras[currentChallengeIndex].size)
                            correctExtraWords.clear()
                            letterPatternView.addLetters(shuffle(challenge[currentChallengeIndex][0]))
                            wordDisplayView.addWords(challenge[currentChallengeIndex])
                            currentLevelDisplay.text = "${(currentChallengeIndex + 1)}/${(size + 1)}"
                        }, 1500
                    )
                }

                return true
            }
        })
    }
    fun giveReward(reward:Int) {
        val preferences = applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE)
        preferences.edit().putInt(
            "coins",
            applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt(
                "coins",
                0
            ) + reward
        ).apply()
        coinsTextView.text = getString(
            R.string.coins, applicationContext.getSharedPreferences(
                "PlayerInfo",
                Context.MODE_PRIVATE
            ).getInt("coins", 0).toString()
        )
    }

    private fun shuffle(input: String): String {
        val characters = ArrayList<Char>()
        for (c in input.toCharArray()) {
            characters.add(c)
        }
        val output = StringBuilder(input.length)
        while (characters.size != 0) {
            val randPicker = (Math.random() * characters.size).toInt()
            output.append(characters.removeAt(randPicker))
        }
        return output.toString()
    }

    private fun isBooleanArrayTrue(boolarray: BooleanArray): Boolean {
        for (i in 0 until boolarray.size) {
            if (!boolarray[i]) {
                return false
            }
        }
        return true
    }

    private fun animateToExtras(start: View, target: View, dur: Int, animatedBoxContainer: LinearLayout) {
        start.post {
            val targetCoordinates = IntArray(2)
            val startCoordinates = IntArray(2)
            target.getLocationOnScreen(targetCoordinates)
            start.getLocationOnScreen(startCoordinates)
            //anim set
            val animset = AnimationSet(false)
            val scale = ScaleAnimation(
                1F,
                0.1F,
                1F,
                0.1F
            )

            val animation = TranslateAnimation(

                0f, (targetCoordinates[0] - startCoordinates[0] + target.width / 2).toFloat(),
                0f, (targetCoordinates[1] - startCoordinates[1] + target.height / 2).toFloat()
            )

            animset.setAnimationListener(object : Animation.AnimationListener {
                val tempBoxContainer = letterPatternView.getChildAt(0) as RelativeLayout
                override fun onAnimationStart(arg0: Animation) {
                    animatedBoxContainer.setBackgroundColor(0)
                }

                override fun onAnimationRepeat(arg0: Animation) {

                }

                override fun onAnimationEnd(arg0: Animation) {
                    for (i: Int in 0 until animatedBoxContainer.childCount) {
                        animatedBoxContainer.getChildAt(i).visibility = View.INVISIBLE

                    }
                    tempBoxContainer.postDelayed({
                        animatedBoxContainer.removeAllViews()
                        tempBoxContainer.removeView(animatedBoxContainer)

                    }, 100)
                }
            })
            animset.addAnimation(scale)
            animset.addAnimation(animation)
            animset.duration = dur.toLong()
            start.startAnimation(animset)
        }


    }

    private fun animateToCorrectAnswer(
        start: View,
        target: View,
        dur: Int,
        containerID: Int,
        animatedBoxContainer: LinearLayout
    ) {
        start.post {
            val targetCoordinates = IntArray(2)
            val startCoordinates = IntArray(2)
            target.getLocationOnScreen(targetCoordinates)
            start.getLocationOnScreen(startCoordinates)
            //anim set
            val animset = AnimationSet(false)
            val scale = ScaleAnimation(
                1F,
                (target.width.toFloat() / start.width.toFloat()),
                1F,
                (target.height.toFloat() / start.height.toFloat())
            )

            val animation = TranslateAnimation(

                0f, (targetCoordinates[0] - startCoordinates[0]).toFloat(),
                0f, (targetCoordinates[1] - startCoordinates[1]).toFloat()
            )

            animset.setAnimationListener(object : Animation.AnimationListener {
                val tempBoxContainer = letterPatternView.getChildAt(0) as RelativeLayout
                override fun onAnimationStart(arg0: Animation) {
                    animatedBoxContainer.setBackgroundColor(0)
                }

                override fun onAnimationRepeat(arg0: Animation) {

                }

                override fun onAnimationEnd(arg0: Animation) {
                    wordDisplayView.wordChangeState(containerID)
                    for (i: Int in 0 until animatedBoxContainer.childCount) {
                        animatedBoxContainer.getChildAt(i).visibility = View.INVISIBLE

                    }
                    tempBoxContainer.postDelayed({
                        animatedBoxContainer.removeAllViews()
                        tempBoxContainer.removeView(animatedBoxContainer)


                    }, 100)
                }
            })
            animset.addAnimation(scale)
            animset.addAnimation(animation)
            animset.duration = dur.toLong()
            start.startAnimation(animset)
        }


    }

    fun storeArray(array: BooleanArray, arrayName: String, mContext: Context) {
        val prefs = mContext.getSharedPreferences("PlayerInfo", 0)
        val editor = prefs.edit()
        for (i in 0 until array.size)
            editor.putBoolean(arrayName + "_" + i, array[i]).apply()
    }

    private fun shuffleToCenter(view: View, dur: Long) {
        val viewparent = view.parent as View
        view.post {
            val animation = TranslateAnimation(
                0f, (viewparent.width / 2 - view.width / 2 - view.x),
                0f, (viewparent.height / 2 - view.height / 2 - view.y)
            )
            animation.duration = dur
            view.startAnimation(animation)
        }

    }

    private fun shuffleToCenterReverse(view: View, dur: Long) {
        val viewparent = view.parent as View
        view.post {
            val animation = TranslateAnimation(
                (viewparent.width / 2 - view.width / 2 - view.x), 0F,
                (viewparent.height / 2 - view.height / 2 - view.y), 0F
            )
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(arg0: Animation) {
                }

                override fun onAnimationRepeat(arg0: Animation) {

                }

                override fun onAnimationEnd(arg0: Animation) {
                    for (i: Int in 1 until letterPatternView.childCount) {
                        letterPatternView.getChildAt(i).visibility = View.VISIBLE
                    }
                }
            })
            animation.duration = dur

//            animation.fillAfter = true
            view.startAnimation(animation)
        }
    }

    private fun shuffleToCenterForAnimationListener(view: View, shuffleString: String) {
        val viewparent = view.parent as View
        view.post {
            val animation = TranslateAnimation(
                0f, (viewparent.width / 2 - view.width / 2 - view.x),
                0f, (viewparent.height / 2 - view.height / 2 - view.y)
            )
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(arg0: Animation) {
                    shuffleButton.isEnabled = false
                }

                override fun onAnimationRepeat(arg0: Animation) {

                }

                override fun onAnimationEnd(arg0: Animation) {
                    letterPatternView.addLetters(shuffle(shuffleString))
                    for (i: Int in 1 until letterPatternView.childCount) {
                        letterPatternView.getChildAt(i).visibility = View.INVISIBLE
                        shuffleToCenterReverse(letterPatternView.getChildAt(i), 500)
                    }
                    shuffleButton.isEnabled = true
                }
            })
            animation.repeatCount = 0
            animation.duration = 500
            animation.fillAfter = true
            // animation.repeatMode = Animation.REVERSE;
            view.startAnimation(animation)

        }

    }

    private lateinit var clickedLine: LinearLayout

    private fun attachOnClickListeners() {
        for (i: Int in 0 until wordDisplayView.childCount) {
            val line = wordDisplayView.getChildAt(i)
            line.setOnClickListener { view ->
                view.startAnimation(AlphaAnimation(1f, 0.4f))
                clickedLine = view as LinearLayout
                if ((getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt("coins", 0) - 5) >= 0
                    && (clickedLine.getChildAt(0) as Box).currentState != State.FILLED
                ) {
                    val intentForResult = Intent(this, HintDialog::class.java)
                    startActivityForResult(intentForResult, 19)


                }
            }
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 19) {
            if (resultCode == Activity.RESULT_OK) {
                if (data?.getBooleanExtra("Answer", false)!!) {
                    val preferences = getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE)

                    while (true) {
                        val random = (Math.random() * clickedLine.childCount).toInt()
                        if ((clickedLine.getChildAt(random) as Box).currentState != State.HINT) {
                            (clickedLine.getChildAt(random) as Box).setState(State.HINT)
                            preferences.edit().putInt(
                                "coins",
                                getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt(
                                    "coins",
                                    0
                                ) - 5
                            ).apply()
                            coinsTextView.text = getString(
                                R.string.coins, getSharedPreferences(
                                    "PlayerInfo",
                                    Context.MODE_PRIVATE
                                ).getInt("coins", 0).toString()
                            )
                            break
                        }
                    }

                }

            }
        }
    }

    override fun onPause() {
        super.onPause()
        letterPatternView.currentBoxContainer?.removeAllViews()
        val parent = letterPatternView.getChildAt(0) as RelativeLayout
        parent.removeView(letterPatternView.currentBoxContainer)
        letterPatternView.currentBoxContainer = null

    }

    override fun onResume() {
        super.onResume()
    }
    override fun onStop()
    {

        letterPatternView.currentBoxContainer?.removeAllViews()
        val parent = letterPatternView.getChildAt(0) as RelativeLayout
        parent.removeView(letterPatternView.currentBoxContainer)
        letterPatternView.currentBoxContainer = null
        super.onStop()
    }

    override fun onDestroy() {

        if (rewardedVideoAd != null) {
            rewardedVideoAd!!.destroy()
            rewardedVideoAd = null
        }
        super.onDestroy()
    }
}

