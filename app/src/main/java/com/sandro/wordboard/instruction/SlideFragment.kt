package com.sandro.wordboard.instruction


import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sandro.wordboard.R
import kotlinx.android.synthetic.main.first_fragment_page.view.*
import org.jetbrains.annotations.Nullable



class SlideFragment : Fragment() {
    private var layoutResId: Int = 0
    private var drawableResId:Int =0
    private var viewId:Int =0

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(ARG_LAYOUT_RES_ID)) {
            layoutResId = arguments!!.getInt(ARG_LAYOUT_RES_ID)
            viewId = arguments!!.getInt(ARG_VIEW_ID)
            drawableResId = arguments!!.getInt(ARG_DRAWABLE_RES_ID)
        }

    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layoutResId, container, false)
        val image = view.findViewById<ImageView>(viewId)
        if(image != null)
        Glide.with(this).load(drawableResId).into(image)
        return view
    }

    companion object {

        private val ARG_LAYOUT_RES_ID = "layoutResId"
        private val ARG_VIEW_ID = "viewId"
        private val ARG_DRAWABLE_RES_ID = "drawableResId"

        fun newInstance(layoutResId: Int,viewId:Int,drawableResId:Int): SlideFragment {
            val sampleSlide = SlideFragment()

            val args = Bundle()
            args.putInt(ARG_LAYOUT_RES_ID, layoutResId)
            args.putInt(ARG_VIEW_ID, viewId)
            args.putInt(ARG_DRAWABLE_RES_ID, drawableResId)
            sampleSlide.arguments = args

            return sampleSlide
        }
    }
}
