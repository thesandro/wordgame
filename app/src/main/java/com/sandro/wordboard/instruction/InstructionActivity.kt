package com.sandro.wordboard.instruction


import android.os.Bundle
import com.sandro.wordboard.R

import android.graphics.Color

import com.github.paolorotolo.appintro.AppIntro
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.first_fragment_page.*
import kotlinx.android.synthetic.main.second_fragment_page.*
import kotlinx.android.synthetic.main.third_fragment_page.*


class InstructionActivity : AppIntro() {
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Todo: Add tutorial gif//video//series of pictures
//

        val firstFragment = SlideFragment.newInstance(R.layout.first_fragment_page,R.id.imageView1,R.drawable.tutorialgif)
        val secondFragment = SlideFragment.newInstance(R.layout.second_fragment_page,R.id.imageView2,R.drawable.boardinstruction)
        val thirdFragment = SlideFragment.newInstance(R.layout.third_fragment_page,R.id.imageView3,R.drawable.patterninstruction)
        addSlide(firstFragment)
        addSlide(secondFragment)
        addSlide(thirdFragment)


        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#efe388"))
        setSeparatorColor(Color.parseColor("#efe3FF"))
        showSeparator(true)

        // Hide Skip/Done button.
        showSkipButton(false)
        isProgressButtonEnabled = true


        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
//        setVibrate(true)
//        setVibrateIntensity(30)
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)

        // Do something when users tap on Skip button.
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        finish()
        // Do something when users tap on Done button.
    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)
        // Do something when the slide changes.
    }

    override fun onBackPressed() {
        finish()
    }
}
