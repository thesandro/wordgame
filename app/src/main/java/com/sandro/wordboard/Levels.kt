package com.sandro.wordboard

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.sandro.wordboard.recycleview.MyAdapter
import kotlinx.android.synthetic.main.activity_levels.*
import android.support.v7.widget.DividerItemDecoration



class Levels : Activity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_levels)
        val res = resources
        val ta = res.obtainTypedArray(R.array.levels)
        val size = ta.length()
        val arrayofnames:ArrayList<String> = ArrayList()
        val maxLevel = applicationContext.getSharedPreferences("PlayerInfo", Context.MODE_PRIVATE).getInt("maxlevel",0)
        ta.recycle()
        // big booboo
        for(i in 1 until size+1)
        {
            arrayofnames.add("Level $i")
        }
        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter(arrayofnames,maxLevel)


        recyclerView = myRecycleView.apply {
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
    }
}
